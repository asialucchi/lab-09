package it.unibo.oop.lab.reactivegui03;
import javax.swing.SwingUtilities;

import it.unibo.oop.lab.reactivegui02.*;

public class AnotherConcurrentGUI extends ConcurrentGUI {

	public AnotherConcurrentGUI() {
		super();
		final Thread t = new Thread(new AnotherAgent());
		t.start();
	}
	
	public class AnotherAgent implements Runnable {
		private final static int TOTAL_TIME = 10;
		private final static int SLEEP_TIME = 1000;
		private int time;
		
		public void run() { 
			while(time < TOTAL_TIME*SLEEP_TIME) {
				try {
					Thread.sleep(SLEEP_TIME);
					time = time + SLEEP_TIME;
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					AnotherConcurrentGUI.this.stop.setEnabled(false);
					AnotherConcurrentGUI.this.up.setEnabled(false);
					AnotherConcurrentGUI.this.down.setEnabled(false);
				}
				
			});
		}
	}

}
