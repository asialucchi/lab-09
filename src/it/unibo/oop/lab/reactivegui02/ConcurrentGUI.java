package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ConcurrentGUI extends JFrame {
	 private static final long serialVersionUID = 1L;
	    private static final double WIDTH_PERC = 0.2;
	    private static final double HEIGHT_PERC = 0.1;
	    private final JLabel display = new JLabel();
	    protected final JButton stop = new JButton("stop");
	    protected final JButton up = new JButton ("up");
	    protected final JButton down = new JButton ("down");
	
	public ConcurrentGUI() {
		super();
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize((int) (screenSize.getWidth() * WIDTH_PERC), (int) (screenSize.getHeight() * HEIGHT_PERC));
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        final JPanel panel = new JPanel();
        panel.add(display);
        panel.add(up);
        panel.add(down);
        panel.add(stop);
        this.getContentPane().add(panel);
        this.setVisible(true);
        
        final Agent agent = new Agent();
        new Thread(agent).start();
        
        up.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agent.setUp();
			}
        });
        
        down.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				agent.setDown();
			}
        });
        
        stop.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		agent.stopAgent();
        		up.setEnabled(false);
        		down.setEnabled(false);
        		stop.setEnabled(false);
        	}
        });
	}
	
	private class Agent implements Runnable{
		
		private volatile boolean stop;
		private volatile boolean up = true;
		private int counter;
		
		@Override
		public void run() {
			while (!this.stop) {
				try {
					SwingUtilities.invokeAndWait(new Runnable() {
						public void run() {
							ConcurrentGUI.this.display.setText(Integer.toString(Agent.this.counter));
						}
					});
					if (this.up) {
						counter++;
					} else {
						counter--;
					}
					Thread.sleep(100);
				} catch(InterruptedException | InvocationTargetException e) {
					ConcurrentGUI.this.display.setText(e.getMessage());
				}
			}
				
		}
		
		public void stopAgent() {
			this.stop = true;
		}
		
		public void setUp() {
			this.up = true;
		}
		
		public void setDown() {
			this.up = false;
		}
	}
	
	

}
